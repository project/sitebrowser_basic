<?php
function  _phptemplate_variables($hook, $vars) {
   $akrmd_vars = array();
   $akrmd_vars['hook']=$hook;
   switch($hook) {
     case 'block' :
        break;
     case 'box' :
        break;
     case 'comment' :
        break;
     case 'node' :
        break;
     case 'page' :
        break;
   }
   if (user_access('access administration pages')) {
     $sitebrowser_vars['is_admin'] = true; 
   }
   
   $vars['sitebrowser_vars'] = $sitebrowser_vars;
   return $vars;
}
?>