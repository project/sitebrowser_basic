<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<!-- HOOK: <?php print $sitebrowser_vars['hook'] ?>-->
<title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  <style type="text/css" media="all">
  <?php if (!($left)) { ?>
  .sitebrowser-middle-bg-c1{background:none;}
  .sitebrowser-middle-column1-outer{display:none;}
  .sitebrowser-middle-column2-outer{margin-left:0;}
  <?php } ?>
  <?php if (!($right)) { ?>
  .sitebrowser-middle-bg-c3{ background:none;}
  .sitebrowser-middle-grouper-1-2{margin-right:0;}
  .sitebrowser-middle-column3-outer{display:none;}                   
  <?php } ?> 
  <?php if (!($sitebrowser_vars['is_admin'])) { ?>
  .sitebrowser-content .submitted,.sitebrowser-content .taxonomy{display:none;}
  .search-results .search-info{display:none;}             
  <?php } ?> 
  </style>
</head>
<body>

<div class="sitebrowser-page-container"><div class="sitebrowser-page-overall-outer"><div class="sitebrowser-page-overall-inner">
  <div class="sitebrowser-header-container"><div class="sitebrowser-header-overall-outer"><div class="sitebrowser-header-overall-inner">
    <div class="sitebrowser-header-column1-outer"><div class="sitebrowser-header-column1-inner">
       <?php if ($logo) { ?>
       <div class="sitebrowser-logo"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a></div>
       <?php } ?>       
       <?php if ($site_name) { ?><div class="sitebrowser-site_name"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></div><?php } ?>
       <?php if ($site_slogan) { ?><div class='sitebrowser-site_slogan'><?php print $site_slogan ?></div><?php } ?> 
    </div></div>
    <div class="sitebrowser-header-column2-outer"><div class="sitebrowser-header-column2-inner">
      <div class="sitebrowser-secondary_links">
      <a href="#skip-navigation">Skip Navigation</a><?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?><?php } ?>
      </div>         
      <div class="sitebrowser-search_box"><?php print $search_box ?><br /></div>
    </div></div>   
  <?php print $header ?></div></div></div>
  <div class="sitebrowser-middle-container"><div class="sitebrowser-middle-overall-outer"><div class="sitebrowser-middle-overall-inner">
  <div class="sitebrowser-middle-bg-c2"><div class="sitebrowser-middle-bg-c3"><div class="sitebrowser-middle-bg-c1">
    <div class="sitebrowser-middle-column3-outer"><div class="sitebrowser-middle-column3-inner">
      <?php if ($right) { ?><div class="sitebrowser-sidebar_right"><?php print $right ?></div><?php } ?>
    </div></div>
    <div class="sitebrowser-middle-grouper-1-2">
      <div class="sitebrowser-middle-column1-outer"><div class="sitebrowser-middle-column1-inner">
        <?php if ($left) { ?><div class="sitebrowser-sidebar_left"><?php print $left ?></div><?php } ?>
      </div></div>
      <div class="sitebrowser-middle-column2-outer"><div class="sitebrowser-middle-column2-inner">      
        <div class="sitebrowser-breadcrumbs-outer"><div class="sitebrowser-breadcrumbs-inner">
           <div class="sitebrowser-breadcrumb"><?php print $breadcrumb ?></div>
        </div></div>        
        <a name="skip-navigation" /><a href="#"></a><!-- empty tag helps prevent weird hovers on following tags -->
        
          <div class="sitebrowser-content-plus">
          <h1><span class="sitebrowser-title"><?php print $title ?></span></h1>
          <div class="sitebrowser-tabs"><?php print $tabs ?></div>
          <div class="sitebrowser-help"><?php print $help ?></div>
          <div class="sitebrowser-messages"><?php print $messages ?></div>
          <div class="sitebrowser-content"><?php print $content; ?></div>          
          </div>
          
          <?php if ($mission) { ?><div class="sitebrowser-mission"><?php print $mission ?></div><?php } ?>
          
      </div></div>
    </div>
  <!-- end middle --><div class="sitebrowser-clear"></div>  
  </div></div></div>  </div></div></div>
  <div class="sitebrowser-footer-container"><div class="sitebrowser-footer-overall-outer"><div class="sitebrowser-footer-overall-inner">
       <?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' => 'links', 'id' => 'subnavlist')) ?><?php } ?>
        <?php if ($footer) { ?><div class="sitebrowser-footer"><?php print $footer ?></div><?php } ?>
        <?php if ($footer_message) { ?><div class="sitebrowser-footer_message"><?php print $footer_message ?></div><?php } ?>
  <!-- end footer --><div class="sitebrowser-clear"></div>  </div></div></div>
  <!--end page--><div class="sitebrowser-clear"></div>  </div></div></div>
<div class="sitebrowser-closure"><?php print $closure ?></div>  


</body>
</html>
<?php
$sitebrowser_vars['page']=ob_get_contents();
ob_end_clean();

//prevent the default term navigation from taking over
if (!($sitebrowser_vars['is_admin'])) {
  $sitebrowser_vars['page'] = str_replace('taxonomy/term','sitebrowser/term',$sitebrowser_vars['page']);
}

//push the Skip Nav link in front of the primary links generated (if they are used)
$sitebrowser_vars['page'] = str_replace('<a href="#skip-navigation">Skip Navigation</a><ul class="links" id="navlist">','<ul class="links" id="navlist"><li class="sitebrowser-first"><a href="#skip-navigation">Skip Navigation</a></li>',$sitebrowser_vars['page']);

echo $sitebrowser_vars['page'];
?>